# Image Scraper
This script is build to scrape all the image files from weburls. It runs with lynx browser to dump the data and download all the images.

## Usage
To run this automated bash script please see following instruction.

```bash
git clone https://gitlab.com/as_abhi6776/image_scraper.git
cd image_scraper
chmod a+x image.sh
sudo ./image.sh   #follow the instructions
```

## Python script
I'm writing python script for the same. Very soon I'll update it.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://mit-license.org/)


# Thanks
