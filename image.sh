#!/bin/bash


APT=$(apt list --installed 2> /dev/null  | grep -ow '\w*lynx\w*')  #it will try to findout whether lynx is installed or not.

if [ ! -n "$APT" ] ;
    then 
        apt install lynx -y #if lynx is not installed in the machine it will try to install
else
    echo " lynx is already  present "
fi

read -p "Please enter the url: " input #userinput for web url
read -p "Please enter your output dir: " dir #it will ask user to give a name to create output directory

#browse and dump all the image url in a txt file called out.txt

lynx -image_links -dump $input |
    grep '\. https\?://.*\.\(gif\|jpg\|png\)$' |
    cut -d . -f 2- |
    cut -d ' ' -f 2- > out.txt

mkdir -p image_down/$dir
cd image_down/$dir

#download all the data using wget from out.txt url file.

wget $(cat ../../out.txt)

echo "images has been successfully downloaded under 'image_down/directoryname' directory"